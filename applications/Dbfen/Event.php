<?php
/**
 * 使用的协议是 文本+回车
 * 测试方法 运行
 * telnet ip 8480
 * 可以开启多个telnet窗口，窗口间可以互相聊天
 * 
 * 
 * @author walkor <walkor@workerman.net>
 */

use \Lib\Context;
use \Lib\Gateway;
use \Lib\StatisticClient;
use \Lib\Store;
use \Protocols\GatewayProtocol;
//use \Protocols\TextProtocol;
use \Protocols\JsonProtocol;
use \Man\Core\Lib\Log;

class Event
{
    /**
     * 当网关有客户端链接上来时触发，每个客户端只触发一次，如果不许要任何操作可以不实现此方法
     * 这里当客户端一连上来就给客户端发送输入名字的提示
     */
    public static function onGatewayConnect($client_id)
    {
        Gateway::sendToCurrentClient(JsonProtocol::encode("type in your name:"));
    }
    
    /**
     * 网关有消息时，判断消息是否完整. 返回0 代表请求接收完成. 返回非0代表没有接收完成. 返回0以后，Gateway进程会将客户端这个请求转发到BusinessWorker处理(onConnect或者onMessage)。
     */
    public static function onGatewayMessage($buffer)
    {
        Log::add("[" . $buffer . "] len = " . strlen($buffer));
        return JsonProtocol::check($buffer);
    }
    
   /**
    * 当WorkerMan接收到客户端发来的一个完整的请求时触发，$client_id是系统自动生成的（大于0的int整型），用来唯一标识客户端。在onMessage里面一般是根据协议解析请求并做处理，如果有需要通过Gateway::sendToAll/sendToClient向其它用户发送消息。
    * @param int $client_id 发消息的client_id
    * @param string $message 消息
    * @return void
    */
   public static function onMessage($client_id, $message)
   {
        $message_data = JsonProtocol::decode($message);
        
        // **************如果没有$_SESSION['name']说明没有设置过用户名，进入设置用户名逻辑************
        if(empty($_SESSION['name']))
        {
            $data = JsonProtocol::decode($message);
            $_SESSION['name'] = $data['name'];
            Gateway::sendToCurrentClient("chat room login success, your client_id is $client_id, name is {$_SESSION['name']}\nuse client_id:words send message to one user\nuse words send message to all\n");
             
            // 广播所有用户，xxx come
            return GateWay::sendToAll(JsonProtocol::encode("{$_SESSION['name']}[$client_id] come"));
        }
        
        // ********* 进入聊天逻辑 ****************
        // 判断是否是私聊
        $explode_array = explode(':', $message, 2);
        // 私聊数据格式 client_id:xxxxx
        if(count($explode_array) > 1)
        {
            $to_client_id = (int)$explode_array[0];
            GateWay::sendToClient($client_id, JsonProtocol::encode($_SESSION['name'] . "[$client_id] said said to [$to_client_id] :" . $explode_array[1]));
            return GateWay::sendToClient($to_client_id, JsonProtocol::encode($_SESSION['name'] . "[$client_id] said to You :" . $explode_array[1]));
        }
        // 群聊
        return GateWay::sendToAll(JsonProtocol::encode($_SESSION['name'] . "[$client_id] said :" . $message));
   }
   
   /**
    * 当用户断开连接时触发的方法
    * @param integer $client_id 断开连接的用户id
    * @return void
    */
   public static function onClose($client_id)
   {
       // 广播 xxx 退出了
       GateWay::sendToAll(JsonProtocol::encode("{$_SESSION['name']}[$client_id] logout"));
   }
}
